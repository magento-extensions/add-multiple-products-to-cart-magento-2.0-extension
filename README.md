[Add Multiple Products to Cart](https://www.softprodigy.com/store/magento2-extensions/add-multiple-product-to-cart) extension is for adding multiple products to the shopping cart. If customers wants to buy multiple products, this extension allows them to do it in proper manner. They can also view product list and insert quantities for each product.

![add multiple products to cart.jpg](https://bitbucket.org/repo/daajj6G/images/2361367800-add%20multiple%20products%20to%20cart.jpg)


**Features:**

* Add multiple products just by single click at any product listing page.
*  Add products at home page.
*  Add product in CMS block.
 
**System Requirement:**

IonCube PHP encoder should be installed on your server.